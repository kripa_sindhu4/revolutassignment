package com.example.revoultassignment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CurrencyItemAdpater.ItemClicked {
    private RecyclerView recyclerView;
    private CurrencyItemAdpater currencyItemAdpater;
    private ArrayList<CountryPojo> countryPojoArrayList;
    private Button errorLayout;
    RequestQueue mRequestQueue;
    private boolean isItemCliked;
    Parcelable recyclerViewState;
    TextView loadingRatesText;
    private int flag[] = {
            R.drawable.aud, R.drawable.bgn, R.drawable.brl,
            R.drawable.cad, R.drawable.chf, R.drawable.cny,
            R.drawable.czk, R.drawable.dkk, R.drawable.gbp,
            R.drawable.hkd, R.drawable.hrk, R.drawable.huf,
            R.drawable.idr, R.drawable.ils, R.drawable.inr,
            R.drawable.isk, R.drawable.jpy, R.drawable.krw,
            R.drawable.mxn, R.drawable.myr, R.drawable.nok,
            R.drawable.nzd, R.drawable.php, R.drawable.pln,
            R.drawable.ron, R.drawable.rub, R.drawable.sek,
            R.drawable.sgd, R.drawable.thb, R.drawable.tryi,
            R.drawable.usd, R.drawable.zar,
    };
   private String[]  currencyCountryName = {
            "Australian Dollar", "Bulgarian Lev", "Brazilian Real",
            "Canadian Dollar", "Swiss Franc", "Yuan Renminbi",
            "Czech Koruna", "Danish Krone", "Pound Sterling",
            "Hong Kong Dollar", "Kuna", "huf",
            "Rupiah", "New Israeli Sheqel", "India",
            "Iceland Krona", "Yen", "Won",
            "Mexican Peso", "Malaysian Ringgit", "Norwegian Krone",
            "nzd", "Philippine Peso", "Zloty",
            "Romanian Leu", "Russian Ruble", "Swedish Krona",
            "Singapore Dolla", "Baht", "Turkish Lira",
            "US Dollar", "Rand",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        errorLayout = (Button) findViewById(R.id.error_layout);
        loadingRatesText = (TextView) findViewById(R.id.loading_rates_text);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);

        // on error hit api
        errorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeJsonObjectRequest();
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isItemCliked = true;
                }
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
        makeJsonObjectRequest();

    }
    @Override
    public void onResume() {
        super.onResume();
        final Handler handler = new Handler();
        final int delay = 1000; //milliseconds

        handler.postDelayed(new Runnable(){
            public void run(){
                if(!isItemCliked){
                  makeJsonObjectRequest();
                }
                handler.postDelayed(this, delay);
            }
        }, delay);

    }

    private void makeJsonObjectRequest() {
        if (!checkInternetConnectivity(MainActivity.this)) {
            Toast.makeText(MainActivity.this,"No Internet connection",Toast.LENGTH_LONG).show();
            return;
        }
        //RequestQueue initialized
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this);
        }

        countryPojoArrayList = new ArrayList<>();

        String url = "https://revolut.duckdns.org/latest?base=EUR";
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject rates = response.getJSONObject("rates");
                    if (rates.length() > 0) {
                        loadingRatesText.setVisibility(View.GONE);
                        errorLayout.setVisibility(View.GONE);
                        Iterator x = rates.keys();
                        int i = 0;
                        int j = 0;
                        while (x.hasNext() && i < flag.length && j< currencyCountryName.length) {
                            String key = (String) x.next();
                            String value = (String) rates.get(key).toString();
                            Drawable  d = getResources().getDrawable(flag[i++]);
                            CountryPojo countryPojo = new CountryPojo(key, value, "", d,currencyCountryName[j++]);
                            countryPojoArrayList.add(countryPojo);
                        }
                        currencyItemAdpater = new CurrencyItemAdpater(countryPojoArrayList, MainActivity.this);
                        recyclerView.setAdapter(currencyItemAdpater);
                    } else {
                        loadingRatesText.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    loadingRatesText.setVisibility(View.GONE);
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                recyclerView.setVisibility(View.GONE);
                loadingRatesText.setVisibility(View.GONE);
                errorLayout.setVisibility(View.VISIBLE);
            }
        });

        // Adding request to request queue
        mRequestQueue.add(jsonObjReq);
    }

    @Override
    public void onItecmClicked(boolean isclickedorTextempty) {
        isItemCliked = isclickedorTextempty;
        recyclerView.smoothScrollToPosition(0);
    }
    public boolean checkInternetConnectivity(Context mContext) {

        if (mContext == null) {
            return false;
        }
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }

    }

}
