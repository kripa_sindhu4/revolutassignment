package com.example.revoultassignment;

import android.graphics.drawable.Drawable;

public class CountryPojo {
    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_currency() {
        return c_currency;
    }

    public void setC_currency(String c_currency) {
        this.c_currency = c_currency;
    }

    public String getEdit_value() {
        return edit_value;
    }

    public void setEdit_value(String edit_value) {
        this.edit_value = edit_value;
    }

    public Drawable getCounty_flag() {
        return county_flag;
    }

    public void setCounty_flag(Drawable county_flag) {
        this.county_flag = county_flag;
    }

    private String c_name;
    private String c_currency;
    private String edit_value;
    private Drawable county_flag;

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    private String country_name;

    public CountryPojo(String c_name, String c_currency, String edit_value, Drawable county_flag,String country_name)
    {
      this.c_name=c_name;
      this.c_currency = c_currency;
      this.edit_value = edit_value;
      this.county_flag = county_flag;
      this.country_name = country_name;
    }



    public CountryPojo( String c_name)
    {
        this.c_name=c_name;
    }
}
