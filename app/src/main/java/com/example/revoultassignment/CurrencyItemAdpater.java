package com.example.revoultassignment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class CurrencyItemAdpater extends RecyclerView.Adapter<CurrencyItemAdpater.MyViewHolder> {

    private ArrayList<CountryPojo> list;
    private boolean onBind;
    private int clickedPostion;
    private Context context;
    private boolean isTextEdited;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView currencyCountry, cuurencyFormat;
        private EditText amountEditText;
        CircleImageView circleImageView;

        private LinearLayout parentLinearLayout, parentLinearLayoutFixed;

        public MyViewHolder(View view) {
            super(view);
            currencyCountry = (TextView) view.findViewById(R.id.currency_country);
            cuurencyFormat = (TextView) view.findViewById(R.id.currency_format);
            amountEditText = (EditText) view.findViewById(R.id.amount_edit_text);
            parentLinearLayout = (LinearLayout) view.findViewById(R.id.parent_layout);
            circleImageView = (CircleImageView) view.findViewById(R.id.country_image);

        }
    }


    public CurrencyItemAdpater(ArrayList<CountryPojo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        CountryPojo countryPojo = list.get(position);
        holder.currencyCountry.setText(countryPojo.getC_name());
        holder.cuurencyFormat.setText(countryPojo.getCountry_name());

        holder.circleImageView.setImageDrawable(countryPojo.getCounty_flag());
        if(!isTextEdited){
            holder.amountEditText.setText("100");
            updateRecycleView(holder.amountEditText.getText().toString().trim(), holder);
        }

        onBind = true;
        holder.amountEditText.setText(countryPojo.getEdit_value());
        onBind = false;
        holder.amountEditText.setSelection(holder.amountEditText.getText().length());

        holder.amountEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction()) {

                    holder.amountEditText.requestFocus();
                    InputMethodManager manager = (InputMethodManager)
                            context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    manager.showSoftInput(holder.amountEditText, 0);
                    notifyItemRangeChanged(position, list.size());
                    Collections.swap(list, position, 0);
                    notifyItemMoved(position, 0);
                    notifyDataSetChanged();
                    clickedPostion = 0;
                    ItemClicked itemClicked = (ItemClicked) context;
                    itemClicked.onItecmClicked(true);
                }

                return true; // return is important...
            }
        });


        holder.amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!onBind) {
                    isTextEdited = true;
                    updateRecycleView(s.toString(), holder);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void updateRecycleView(String editTextValue, MyViewHolder holder) {
        try{
        if (list != null && list.size() > 0) {
            if (clickedPostion == 0) {
                CountryPojo countryPojo = list.get(0);
                countryPojo.setEdit_value(editTextValue);
            }
            for (int i = 1; i < list.size(); i++) {
                CountryPojo countryPojo = list.get(i);
                {
                    DecimalFormat df = new DecimalFormat("#,###.00");
                    if (!TextUtils.isEmpty(editTextValue)) {
                        Double value = Double.parseDouble(countryPojo.getC_currency()) * Double.parseDouble(editTextValue);
                        countryPojo.setEdit_value(df.format(value));
                    } else {
                        countryPojo.setEdit_value("");
                    }

                }
            }
            notifyDataSetChanged();

        }}catch (Exception  e){
            e.printStackTrace();
        }
    }

    public static interface ItemClicked {
        public void onItecmClicked(boolean isclickedorTextempty);
    }


}
